tap_name=tap-cocktail-db
catalog_path=catalog.json
config_path=config.json



get_catalog:
	tap-cocktail-db  -c ${config_path} --discover > ${catalog_path}
	python3 scripts/create_selected_schemas.py

sync:
	@tap-cocktail-db -c ${config_path} --catalog  ${catalog_path}

install:
	pip3 install -U -e .

venv:
	python3 -m venv ~/.virtualenvs/${tap_name}
	source ~/.virtualenvs/${tap_name}/bin/activate

check_tap:
	tap-cocktail-db -c ${config_path} --catalog  ${catalog_path} | singer-check-tap
