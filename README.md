# tap-cocktail-db

### *Not quite CockroachDB but as close as I could get in a weekend*

This is a [Singer](https://singer.io) tap that produces JSON-formatted data
following the [Singer
spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

This tap uses [TheCocktailDB.com api](https://www.thecocktaildb.com/) to search drinks by name.

Simply adjust the config file to search by different keywords


## Set up Virtual Environment

run "make venv"

## Install Tap

run "make install"

## Run in Discovery Mode

run "make get_catalog"

*This make rule will also run a script for modifying the catalog to select stream and fields*

## Check Validity of Schema

Run "make check_tap"

## Run in Sync Mode

run "make sync"


---

Copyright &copy; 2018 Stitch
