from datetime import datetime
import pytz

input_date = '2015-08-18 14:42:59'

date_object = datetime.strptime(input_date, "%Y-%m-%d %H:%M:%S")
print(date_object)

d_with_timezone = date_object.replace(tzinfo=pytz.UTC)
output = d_with_timezone.isoformat()
print(output)
