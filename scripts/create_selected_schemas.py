'''

Should look similar to the one here


Extend later by by looping through looping

https://github.com/singer-io/getting-started/blob/master/docs/DISCOVERY_MODE.md#schemas
'''
import os
import json
import pprint

path_to_catalog = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'catalog.json')

with open(path_to_catalog) as f:
    catalog = json.load(f)

catalog['streams'][0]['metadata'].append(
    {
        "metadata": {
            "inclusion": "available",
            "table-key-properties": ["idDrink"],
            "selected": True,
            "valid-replication-keys": ["dateModified"],
            "schema-name": catalog['streams'][0]['stream'],
        },
        "breadcrumb": []
    }
)

properties = catalog['streams'][0]['schema']['properties'].keys()
for i in properties:
    catalog['streams'][0]['metadata'].append(
        {
            "breadcrumb": ["properties", i],
            "metadata": {
                "inclusion": "automatic",
                "selected": "true"
            }
        }
    )

with open(path_to_catalog, 'w') as f:
    json.dump(catalog, f, indent=4)
