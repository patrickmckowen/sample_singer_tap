'''
A script for testing the endpoint and observing the data
'''
import requests

def tap_data(beverage):
    response = requests.request(
        "GET",
        "https://www.thecocktaildb.com/api/json/v1/1/search.php",
        params={"s": beverage}
    )
    #print(response.json())
    return response.json()

data = tap_data('margarita')
print(len(data))
print(data)
