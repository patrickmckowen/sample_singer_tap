#!/usr/bin/env python
from setuptools import setup

setup(
    name="tap-cocktail-db",
    version="0.1.0",
    description="Singer.io tap for extracting data",
    author="Stitch",
    url="http://singer.io",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_cocktail_db"],
    install_requires=[
        # NB: Pin these to a more specific version for tap reliability
        "singer-python",
        "requests",
        "pytz",
        "singer-tools",
    ],
    entry_points="""
    [console_scripts]
    tap-cocktail-db=tap_cocktail_db:main
    """,
    packages=["tap_cocktail_db"],
    package_data = {
        "schemas": ["tap_cocktail_db/schemas/*.json"]
    },
    include_package_data=True,
)
